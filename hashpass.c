#include <stdio.h>
#include "md5.h"
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    FILE *file1 = fopen(argv[1], "r");
    if(!file1)
    {
        printf("cant open %s for reading\n", argv[2]);
        exit(1);
    }
    
    FILE *file2 = fopen(argv[2], "w");
    if(!file2)
    {
        printf("cant open %s for writing\n", argv[2]);
        exit(1);
    }
    
    char *str = malloc(150);
    char *hash = malloc(150);
    while(!feof(file1))
    {
        
        fgets(str, (strlen(argv[1])), file1);
        hash = md5(str, strlen(str)-1);
        fprintf(file2, "%s\n", hash);
    }
 
    free(hash);
    free(str);
    fclose(file1);
    fclose(file2);
    
    
}

